import requests
import json

url      = "http://nginx/api/entreprises" #nginx est le nom d'host du conteneur founissant l'application web
token    = "tokenargenty"
donnees  = {
            'name': 'pythonentreprise'
           }
en_tetes = {
            'X-Requested-With': 'XMLHttpRequest',
            'Authorization': 'Bearer ' + token
           }

#response = requests.post(url, headers=en_tetes, data=donnees).text #ajout
response = requests.get(url,  headers=en_tetes).text #index
#response = requests.delete(url+'/5', headers=en_tetes).text #delete
#response = requests.put(url+'/4', headers=en_tetes, data=donnees).text #update


response_json = json.loads(response)
print(json.dumps(response_json, indent=4))