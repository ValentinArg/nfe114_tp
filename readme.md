# -- FOR TP ONLY -- Dépot du TP de cours -- FOR TP ONLY --

Ne pas utiliser en production ou autre, certains éléments (clé, mots de passe...) étant **PUBLICS** !!

## Installation

1. Cloner le dépot \
    `git clone https://gitlab.com/ValentinArg/nfe114_tp.git`
1. lancer les conteneurs: \
    `docker-compose up -d --build`
1. Installation des dépendances : \
    `docker run --rm -v $pwd/my-site:/app composer update` \
    NB: `$pwd` permet de récuperer le chemin absolu du dossier courant. Cette commande peut varier selon votre système.
1. Patienter...
1. Tester avec un navigateur: http://localhost

## Utiliser Laravel Artisan

1. ouvrir une console PHP depuis le conteneur: \
    `docker exec -it php /bin/sh -c "[ -e /bin/bash ] && /bin/bash || /bin/sh"` \
    NB: *php* est la valeur de l'attribut *container_name* pour le service PHP dans le fichier *docker-compose.yml*
1. lancer la commande: \
    `php artisan <ce dont vous avez besoin>`