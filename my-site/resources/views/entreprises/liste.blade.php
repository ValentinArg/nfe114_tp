@extends('layouts.default')

@section('title', 'Liste des entreprises')

@section('contenu')
<h1>Liste des entreprises</h1>

<ul>
@foreach($entreprises as $entreprise)
    <li>
        id:         {{$entreprise['id']}}
        nom:        {{$entreprise['name']}}
        <a href="/entreprises/{{$entreprise['id']}}">détails</a>
        <form action="/entreprises/{{$entreprise['id']}}" method="post">
            <input type="hidden" name="_method" value="delete">
            @csrf
            <input type="submit" value="supprimer">
        </form>
    </li>
@endforeach
</ul>
<p>
    <a href="/entreprises/create">Ajouter un entreprise</a>
</p>

@endsection