axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'

Vue.component('entreprises', {
    data: function () {
        return {
            input_entreprises: '',
            entreprises: [
            ]
        }
    },
    template : `<div>
                    <input type="text"   v-model="input_entreprises">
                    <input type="button" value="Ajouter" @click="addEntreprise(input_entreprises)">
                    <p>{{input_entreprises}}</p>
                    <ul v-if="entreprises.length > 0">
                        <li v-for="(entreprise, index) in entreprises">
                            {{ entreprise }} <input type="button" value="Supprimer" @click="deleteEntreprise(index)">
                        </li>
                    </ul>
                    <p v-else> Liste est vide. </p>
                </div>`,
    methods: {
        addEntreprise: function (p_entreprise) {
            //this.entreprises.push(p_entreprise);
            axios.post('entreprises',
                {'name':p_entreprise}
            ).then(response => {this.entreprises = response.data})
        },
        deleteEntreprise: function (p_index) {
            //this.entreprises.splice(p_index, 1)
            axios.delete('entreprises/' + p_index).then(response => {this.entreprises = response.data})
        }
    },
    mounted: function(){
        axios.get('entreprises').then(response => {
            this.entreprises = response.data;
        });
    }
  });

let app = new Vue({
     el: '#app'
});
