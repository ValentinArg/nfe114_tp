<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Group;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $groups = Group::all();
        if($request->ajax()){
            return Group::all();
        }
        return view('groupes.liste', ['groupes' => $groups]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('groupes.formAjout');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $group       = new Group();
        $group->name = $request->name;
        $group->save();
        if($request->ajax()){
            return Group::all();
        }
        return view('groupes.liste', ['groupes' => Group::all()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('groupes.show', ['groupe' => Group::find($id)->toArray()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $groupe = Group::find($id);
        return view('groupes.formEdit', ['groupe' => $groupe]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $groupe       = Group::find($id);
        $groupe->name = $request->name;
        $groupe->save();
        if($request->ajax()){
            return Group::all();
        }
        return view('groupes.liste', ['groupes' => Group::all()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $groupe = Group::find($id);
        $groupe->delete();
        if($request->ajax()){
            return Group::all();
        }
        return view('groupes.delete', ['groupes' => Group::all()]);
    }
}
